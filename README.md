# Dashboard

This is a base package to use to quickly start a project. Functionality and workflow for events, emails, listeners have all been left out of this package so you can do whatever fits your project.