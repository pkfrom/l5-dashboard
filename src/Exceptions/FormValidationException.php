<?php

/**
 * @package     Dashboard
 * @author      Ian Olson <me@ianolson.io>
 * @license     MIT
 * @copyright   2015, Fromz
 * @link        https://github.com/laraflock
 */

namespace Fromz\Dashboard\Exceptions;

use Exception;

class FormValidationException extends Exception
{
    protected $errors;

    public function __construct($message = '', $errors = [])
    {
        $this->errors = $errors;

        parent::__construct($message);
    }

    public function getErrors()
    {
        return $this->errors;
    }
}