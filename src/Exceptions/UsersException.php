<?php

/**
 * @package     Dashboard
 * @author      Ian Olson <me@ianolson.io>
 * @license     MIT
 * @copyright   2015, Fromz
 * @link        https://github.com/laraflock
 */

namespace Fromz\Dashboard\Exceptions;

use Exception;

class UsersException extends Exception
{
    public function __construct($message = '')
    {
        parent::__construct($message);
    }
}