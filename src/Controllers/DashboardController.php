<?php

/**
 * @package     Dashboard
 * @author      Ian Olson <me@ianolson.io>
 * @license     MIT
 * @copyright   2015, Fromz
 * @link        https://github.com/laraflock
 */

namespace Fromz\Dashboard\Controllers;

class DashboardController extends BaseDashboardController
{
    /**
     * The dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function dashboard()
    {
        return $this->view('dashboard.index');
    }
}